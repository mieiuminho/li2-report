PROJECT    = report
BIB        = --filter pandoc-citeproc --bibliography=references.bib
SRC        = $(PROJECT).md sections/introduction.md sections/implementation.md sections/conclusion.md
TEX        = --template styles/template.tex --highlight-style tango
HTML       = --template styles/template.html --css styles/style.css

pdf:
	pandoc $(TEX) $(BIB) -s $(SRC) -o $(PROJECT).pdf

html:
	pandoc $(HTML) $(BIB) -s $(SRC) -o $(PROJECT).html

setup:
	sudo apt-get update
	sudo apt-get install pandoc pandoc-citeproc pandoc-include texlive-full

clean:
	@echo "Cleaning..."
	@curl https://raw.githubusercontent.com/nelsonmestevao/spells/master/art/maid.ascii
	@rm -rf $(PROJECT).pdf $(PROJECT).html
	@echo "...✓ done!"
