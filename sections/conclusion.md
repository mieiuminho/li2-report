# Conclusão

Para concluir, o algoritmo base do nosso trabalho para tabuleiros fáceis é:
gerar algumas peças fixas e bloqueadas no tabuleiro, jogar o máximo número de
jogadas óbvias possível, se o tabuleiro ainda não estiver completo gerar uma
nova peça fixa e repetir o processo depois do 1º passo.

Já nos tabuleiros difíceis é: jogar o máximo número de jogadas óbvias possível,
se o tabuleiro não estiver cheio jogar uma peça a partir de uma suposição, se
nenhuma peça for jogada na suposição e o tabuleiro não estiver completo gerar
uma nova peça fixa e voltar ao primeiro passo, senão voltar ao primeiro passo.
No final de ambos os algoritmos as peças que não são fixas ou bloqueadas são
retiradas.

Fazendo uma pequena análise relativa às competências adquiridas durante a
realização deste trabalho conseguimos concluir que o mesmo foi bastante
importante no que diz respeito à aquisição de conhecimentos sobre algoritmos,
tendo sido a nossa primeira introdução séria a essa área.

Achamos também que alcançamos os objetivos desta fase do trabalho, ou seja,
conseguimos gerar com sucesso tabuleiros resolúveis de dificuldades distintas,
conforme a vontade do utilizador.

De uma maneira geral é nossa convicção que atingimos os objetivos propostos
aquando da apresentação deste trabalho.
